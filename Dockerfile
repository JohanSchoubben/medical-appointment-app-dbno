FROM openjdk:11
COPY target/dbno.jar dbno.jar
EXPOSE 8181
ENTRYPOINT ["java","-jar","dbno.jar"]