package be.multimedi.latte.dbno.bootstrap;

import be.multimedi.latte.dbno.model.Calendar;
import be.multimedi.latte.dbno.model.Event;
import be.multimedi.latte.dbno.model.Status;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CalculationHelperClass {

    public static long calculateNumberOfEvents(long minutesPerPatient,
                                               LocalDateTime startTimeOfPeriod,
                                               LocalDateTime endTimeOfPeriod) {
        long timeBlock = Duration.between(startTimeOfPeriod, endTimeOfPeriod).toMinutes();
        return timeBlock / minutesPerPatient;
    }

    public static void addTimeslotsToList(long numberOfSlots,
                                          LocalDateTime timeBlockStartTime,
                                          long minutesPerPatient,
                                          List<LocalDateTime> timeslotsList) {

        for (int i = 1; i <= numberOfSlots; i++) {
            timeslotsList.add(timeBlockStartTime);
            timeBlockStartTime = timeBlockStartTime.plusMinutes(minutesPerPatient);
        }
    }

    public static List<Event> addEventsToList(List<LocalDateTime> timeslots,
                                              Calendar calendar) {
        List<Event> eventsToAddToCalendar = new ArrayList<>();
        for (LocalDateTime slot : timeslots) {
            Event event = new Event();
            event.setTitle("Consultatie");
            event.setDate(slot.toLocalDate());
            event.setStartTime(slot.toLocalTime());
            event.setStatus(Status.AVAILABLE);
            event.setCalendar(calendar);
            eventsToAddToCalendar.add(event);
        }
        return eventsToAddToCalendar;
    }
}
