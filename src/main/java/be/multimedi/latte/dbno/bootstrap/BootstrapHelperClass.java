package be.multimedi.latte.dbno.bootstrap;

import be.multimedi.latte.dbno.model.Address;
import be.multimedi.latte.dbno.model.Gender;
import be.multimedi.latte.dbno.model.User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class BootstrapHelperClass {

    private static final String[][] addressData = {
            {"Meiboomlaan", "17", "Tongeren", "3700"},
            {"Torenstraat", "32", "Wommelgem", "2160"},
            {"Liersesteenweg", "135", "Heist-op-den-Berg", "2220"},
            {"Universiteitslaan", "306", "Diepenbeek", "3590"},
            {"Weg naar Zutendaal", "26", "Maasmechelen", "3630"},
            {"Koksijdesteenweg", "158", "Koksijde", "8670"},
            {"Dusaertplein", "53", "Hasselt", "3500"},
            {"Brustemsesteenweg", "402", "Sint-Truiden", "3800"},
            {"Jan van Rijswijcklaan", "133", "Antwerpen", "2018"},
            {"Blandenstraat", "26", "Haasrode", "3053"},
            {"Dorpsplein", "16", "Sint-Katelijne-Waver", "2860"},
            {"Steenweg op Mollem", "125", "Asse", "1730"},
            {"Turnhoutsebaan", "95", "Schilde", "2970"},
            {"Korenmarkt", "11", "Gent", "9000"},
            {"Javaplein", "38", "Schaarbeek", "1030"}
    };

    private static final String[][] userData = {
            {"Ruben", "Fobe", "0470123456", "ruben@mail.be", "abcd1234A&", "MALE"},
            {"Nazha", "Rabah", "0470123457", "nazha@mail.be", "bcde2345B&", "FEMALE"},
            {"Stef", "Vanoppen", "0470123458", "stef@mail.be", "cdef3456C!", "MALE"},
            {"Abdulvahap", "Karabekmez", "0470123459", "abdulvahap@mail.be", "defg4567D&", "MALE"},
            {"Johan", "Schoubben", "0470123460", "johan@mail.be", "efgh5678E&", "MALE"},
            {"Ben", "Degilim", "0480987654", "ben@mail.be", "fhgi6789F&", "MALE"},
            {"Nour", "El Karoui", "0480987653", "nour@mail.be", "hgij7890G&", "FEMALE"},
            {"Azra", "Yildirim", "0480987653", "azra@mail.be", "gijk8901H&", "FEMALE"},
            {"Richard", "Coeur de Lion", "0480987652", "richard@mail.be", "ijkl9012I&", "MALE"},
            {"Clint", "Eastwood", "0480987651", "clint@mail.be", "jklm0123J&", "MALE"},
            {"Bram", "Depoortere", "0480987650", "bram@mail.be", "klmn1234K&", "MALE"},
            {"Mieke", "Troosters", "0480987649", "mieke@mail.be", "lmno2345L&", "FEMALE"},
            {"Eva", "Mendez", "0480987648", "eva@mail.be", "mnop3456M!", "FEMALE"},
            {"Adelina", "Abdallah", "0480987647", "adelina@mail.be", "nopq4567N&", "FEMALE"},
            {"Yannick", "Van Ham", "0480987646", "yannick@mail.be", "opqr5678O&", "MALE"}
    };

    public static List<Address> generateAddressList() {
        List<Address> addresses = new ArrayList<>();
        for (String[] el : addressData) {
            Address address = new Address();
            address.setCountry("België");
            address.setStreet(el[0]);
            address.setHouseNumber(Integer.parseInt(el[1]));
            address.setCity(el[2]);
            address.setZipcode(el[3]);
            addresses.add(address);
        }
        return addresses;
    }

    private static final List<Address> newAddresses = generateAddressList();

    public static List<User> generateUserList() {
        List<User> users = new ArrayList<>();
        int index = 0;
        for (String[] elem : userData) {
            User user = new User();
            user.setFirstName(elem[0]);
            user.setLastName(elem[1]);
            user.setDob(LocalDate.of(1991, 10, 1));
            user.setEnabled(true);
            user.setPhoneNumber(elem[2]);
            user.setEmail(elem[3]);
            user.setPassword(elem[4]);
            user.setGender(Gender.valueOf(elem[5]));
            user.setNationalRegisterNumber("91100163841");
            user.setAddress(newAddresses.get(index));
            index++;
            users.add(user);
        }
        return users;
    }

    public static List<Address> generateOfficeAddresses() {
        List<Address> officeAddresses = new ArrayList<>();
        for (int i = 3; i < addressData.length; i++) {
            Address address = new Address();
            address.setCountry("België");
            address.setStreet(addressData[i][0]);
            address.setHouseNumber(Integer.parseInt(addressData[i][1]));
            address.setCity(addressData[i][2]);
            address.setZipcode(addressData[i][3]);
            officeAddresses.add(address);
        }
        return officeAddresses;
    }

    private static final LocalDateTime[] period1 = {
            LocalDateTime.of(2021, 7, 24, 8, 0),
            LocalDateTime.of(2021, 7, 24, 12, 0)};
    private static final LocalDateTime[] period2 = {
            LocalDateTime.of(2021, 7, 26, 11, 0),
            LocalDateTime.of(2021, 7, 26, 16, 0)};
    private static final LocalDateTime[] period4 = {
            LocalDateTime.of(2021, 7, 27, 17, 0),
            LocalDateTime.of(2021, 7, 27, 19, 30)};
    private static final LocalDateTime[] period5 = {
            LocalDateTime.of(2021, 7, 28, 13, 0),
            LocalDateTime.of(2021, 7, 28, 18, 0)};
    private static final LocalDateTime[] period6 = {
            LocalDateTime.of(2021, 7, 29, 10, 0),
            LocalDateTime.of(2021, 7, 29, 14, 0)};
    private static final LocalDateTime[] period7 = {
            LocalDateTime.of(2021, 7, 30, 13, 0),
            LocalDateTime.of(2021, 7, 30, 18, 0)};
    private static final LocalDateTime[] period8 = {
            LocalDateTime.of(2021, 7, 31, 8, 0),
            LocalDateTime.of(2021, 7, 31, 12, 0)};

    private static final LocalDateTime[][] availability =
            {period1, period2, period4, period5, period6, period7, period8};

    public static LocalDateTime[][] getAvailability() {
        return availability;
    }
}
