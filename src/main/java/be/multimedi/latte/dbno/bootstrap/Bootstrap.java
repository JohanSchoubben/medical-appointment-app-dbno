package be.multimedi.latte.dbno.bootstrap;


import be.multimedi.latte.dbno.model.*;
import be.multimedi.latte.dbno.repository.CalendarRepository;
import be.multimedi.latte.dbno.repository.OfficeRepository;
import be.multimedi.latte.dbno.repository.RoleRepository;
import be.multimedi.latte.dbno.service.DoctorService;
import be.multimedi.latte.dbno.service.EventService;
import be.multimedi.latte.dbno.service.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static be.multimedi.latte.dbno.bootstrap.BootstrapHelperClass.*;
import static be.multimedi.latte.dbno.bootstrap.CalculationHelperClass.*;

@Component
@Profile("test")
@RequiredArgsConstructor
public class Bootstrap {

    private final DoctorService doctorService;
    private final PatientService patientService;
    private final RoleRepository roleRepository;
    private final EventService eventService;
    private final CalendarRepository calendarRepository;
    private final OfficeRepository officeRepository;

    @PostConstruct
    public void init() {

        List<Role> roles = Arrays.asList(new Role("USER"), new Role("STAFF"), new Role("ADMIN"));
        roleRepository.saveAll(roles);

        List<User> users = generateUserList();
        Specialization[] specializations = Specialization.values();

        for (int i = 0; i < 3; i++) {
            Patient patient = new Patient();
            patient.setUser(users.get(i));
            patientService.register(patient);
        }

        List<Doctor> doctors = new ArrayList<>();
        for (int i = 3, j = 0; i < users.size(); i++) {
            Doctor doctor = new Doctor();
            doctor.setUser(users.get(i));
            if (j < specializations.length) {
                doctor.setSpecialization(specializations[j]);
            } else {
                doctor.setSpecialization(specializations[j - specializations.length]);
            }
            j++;
            doctors.add(doctor);
            doctorService.register(doctor);
        }

        List<Address> addresses = generateOfficeAddresses();
        for (int i = 0; i < doctors.size(); i++) {
            Office office = new Office();
            office.setName("Privépraktijk Dr. " + doctors.get(i).getUser().getLastName());
            office.setAddress(addresses.get(i));
            office.setDoctors(List.of(doctors.get(i)));
            officeRepository.save(office);
            doctors.get(i).setOffices(List.of(office));
            doctorService.editDoctor(doctors.get(i));
        }

        long minutesPerPatient = 15L;
        long numberOfEvents;
        LocalDateTime startOfCalculation;
        List<LocalDateTime> timeslots = new ArrayList<>();
        LocalDateTime[][] availability = getAvailability();

        for (int i = 0; i < availability.length; i++) {
            numberOfEvents = calculateNumberOfEvents(minutesPerPatient, availability[i][0], availability[i][1]);
            startOfCalculation = availability[i][0];
            addTimeslotsToList(numberOfEvents, startOfCalculation, minutesPerPatient, timeslots);
        }

        List<Calendar> calendars = new ArrayList<>();
        for (Doctor doctor : doctors) {
            Calendar calendar = new Calendar();
            calendar.setDoctor(doctor);
            calendar.setName("Dr. " + doctor.getUser().getLastName() + " - " + doctor.getSpecialization().getValue());
            calendarRepository.save(calendar);
            calendars.add(calendar);
            doctor.setCalendar(calendar);
            doctorService.editDoctor(doctor);
        }

        for (Calendar calendar : calendars) {
            List<Event> events = addEventsToList(timeslots, calendar);
            calendar.getDoctor().setCalendar(calendar);
            doctorService.editDoctor(calendar.getDoctor());
            eventService.saveAllEvents(events);
        }
    }
}

