package be.multimedi.latte.dbno.controller;


import be.multimedi.latte.dbno.model.User;
import be.multimedi.latte.dbno.security.VerificationToken;
import be.multimedi.latte.dbno.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import java.security.Principal;
import java.util.Calendar;
import java.util.Locale;

@Controller
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/my-profile")
    public String myProfileView(Model model, Principal principal) {
        User loggedInUser = userService.retrieveByEmail(principal.getName());
        model.addAttribute("user", loggedInUser);
        return "myProfile";
    }


    @GetMapping("/registrationConfirm")
    public String confirmRegistration(WebRequest request, Model model, @RequestParam("token") String token) {
        Locale locale = request.getLocale();
        VerificationToken verificationToken = userService.getVerificationToken(token);
        if (verificationToken == null) {
            String message = "invalid token";
            model.addAttribute("message", message);
            return "redirect:/badUser.html";
        }
        User user = verificationToken.getUser();
        Calendar calendar = Calendar.getInstance();
        if((verificationToken.getExpiryDate().getTime()-calendar.getTime().getTime())<= 0){
            String messageValue = "Expired , you were too slow to validate.";
            model.addAttribute("message", messageValue);
            return "redirect:/badUser.html";

        }

        user.setEnabled(true);
        userService.saveRegisteredUser(user);
        return "redirect:/login";
    }

}
