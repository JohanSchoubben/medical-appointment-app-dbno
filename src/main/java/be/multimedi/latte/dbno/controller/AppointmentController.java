package be.multimedi.latte.dbno.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class AppointmentController {


    @GetMapping("/dr-appointment")
    public String fetchDrAppointment(){
        return "dr-appointment";
    }
}
