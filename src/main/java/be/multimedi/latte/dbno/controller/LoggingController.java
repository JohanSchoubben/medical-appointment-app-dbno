package be.multimedi.latte.dbno.controller;


//controller for logging in and out

import be.multimedi.latte.dbno.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
@AllArgsConstructor

public class LoggingController {

    private final UserService userService;

    @GetMapping("/login")
    public String login(){
        return "login";
    }


    @GetMapping("/logout")
    public String logout(){
        return "home";
    }


}
