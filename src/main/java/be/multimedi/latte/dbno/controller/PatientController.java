package be.multimedi.latte.dbno.controller;


import be.multimedi.latte.dbno.DTO.PatientDto;
import be.multimedi.latte.dbno.model.*;
import be.multimedi.latte.dbno.repository.CalendarRepository;
import be.multimedi.latte.dbno.security.VerificationToken;
import be.multimedi.latte.dbno.service.*;
import be.multimedi.latte.dbno.util.OnRegistrationCompleteEvent;
import be.multimedi.latte.dbno.util.UserAlreadyExistException;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.lang.Nullable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

@Controller
@RequiredArgsConstructor
@RequestMapping("/patient")
public class PatientController {

    private final PatientService patientService;
    private final DoctorService doctorService;
    private final UserService userService;
    private final PasswordEncoder encoder;
    private final CalendarRepository calendarRepository;
    private final EventService eventService;
    private final EmailService emailService;

//    @GetMapping("/register")
//    public String registerViewPatient(Model model) {
//        model.addAttribute("patient", new Patient());
//        return "patientRegister";
//   }

    @GetMapping("/register")
    public String registerViewPatient(WebRequest request, Model model) {
        PatientDto patientDto = new PatientDto();
        model.addAttribute("patient", patientDto);
        return "patientRegister";
    }

//    @PostMapping("/register")
//    public String registerPatient(@ModelAttribute("patient") @Valid final Patient patient, final BindingResult bindingResult) {
//
//        if (bindingResult.hasErrors()) {
//            return "patientRegister";
//        }
//        patientService.register(patient);
//        return "redirect:/login";
//    }


    @Autowired
    ApplicationEventPublisher eventPublisher;

    @PostMapping("/register")
    public ModelAndView registerUserAccount(
            @ModelAttribute("user") @Valid PatientDto patientDto,
            HttpServletRequest request, Errors errors) {

        try {
            Patient registered = patientService.registerNewUserAccount(patientDto);

            String appUrl = request.getContextPath();
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered.getUser(),
                    request.getLocale(), appUrl));
        } catch (UserAlreadyExistException uaeEx) {
            ModelAndView mav = new ModelAndView("patientRegister", "patient", patientDto);
            mav.addObject("message", "An account for that username/email already exists.");
            return mav;
        } catch (RuntimeException ex) {
            ex.printStackTrace();
            return new ModelAndView("emailError", "patient", patientDto);
        }

        return new ModelAndView("home", "patient", patientDto);
    }


    @GetMapping("/regitrationConfirm")
    public String confirmRegistration
            (WebRequest request, Model model, @RequestParam("token") String token) {

        Locale locale = request.getLocale();

        VerificationToken verificationToken = userService.getVerificationToken(token);
        if (verificationToken == null) {
            return "redirect:/badUser.html";
        }

        User user = verificationToken.getUser();
        Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            return "redirect:/badUser.html";
        }
        user.setEnabled(true);
        userService.saveRegisteredUser(user);
        return "redirect:/login";
    }


    @GetMapping("/search")// /search?name=abc & spec=cbd
    public String doctorOverview(final Model model, @RequestParam @Nullable final String name, @RequestParam @Nullable final Specialization spec) {

        List<Doctor> doctors = doctorService.search(name, spec);
        model.addAttribute("doctors", doctors);

        return "/doctorList";
    }

    @GetMapping("/my-profile")
    public String myProfileView(Model model, Principal principal) {
        User loggedInUser = userService.retrieveByEmail(principal.getName());
        Patient patient = patientService.retrieveByUser(loggedInUser);
        model.addAttribute("patient", patient);
        return "patient/myProfile";
    }

    @GetMapping("/my-profile/edit")
    public String editProfileView(Model model, Principal principal) {
        User loggedInUser = userService.retrieveByEmail(principal.getName());
        Patient patient = patientService.retrieveByUser(loggedInUser);
        patient.getUser().setPassword("");
        model.addAttribute("patient", patient);
        return "patient/profileForm";
    }

    @PostMapping("/my-profile/edit")
    public String updateProfile(@ModelAttribute("patient") @Valid Patient updatedPatient, BindingResult br, Principal principal) { //Principal = is information about 'current logged in user'
        if (br.hasErrors()) {
            return "patient/profileForm";
        }
        User loggedInUser = userService.retrieveByEmail(principal.getName());
        Patient patient = patientService.retrieveByUser(loggedInUser);
        String oldPassword = updatedPatient.getUser().getPassword();
        String newPassword = encoder.encode(oldPassword);
        updatedPatient.getUser().setPassword(newPassword);
        boolean allowedToUpdate = patient.getId().equals(updatedPatient.getId());
        if (allowedToUpdate) {
            patientService.updateUser(updatedPatient);
        }
        return "redirect:/my-profile?updated";
    }

    @GetMapping("visit/{id}")
    public String visitDoctor(@PathVariable Long id, Model model) {
        Doctor doctor = doctorService.findDoctorById(id);
        model.addAttribute("doctor", doctor);
        return "doctor/dr-detail";
    }

    @GetMapping("/calendar/{doctorId}")
    public String showDrCalendar(Model model, @PathVariable Long doctorId) {
        Doctor doctor = doctorService.findDoctorById(doctorId);
        be.multimedi.latte.dbno.model.Calendar calendar = calendarRepository.findByDoctor(doctor);
        //added ******
        List<Event> allEvents = eventService.findAllByCalendarAndStatus(calendar, Status.AVAILABLE);

        //Info to fill out calendar.
        JsonArray eventArray = new JsonArray();

        for (int i = 0; i < allEvents.size(); i++) {
            JsonObject eventJson = new JsonObject();
            eventJson.addProperty("title", allEvents.get(i).getTitle());
            eventJson.addProperty("start", allEvents.get(i).getDate() + "T" +
                    allEvents.get(i).getStartTime());
            eventJson.addProperty("end", allEvents.get(i).getDate() + "T" +
                    allEvents.get(i).getEndTime());
            eventJson.addProperty("status", allEvents.get(i).getStatus().name());
            // eventJson.addProperty("patient", allEvents.get(i).getPatient().getUser().getEmail());
            eventArray.add(eventJson);
        }
        model.addAttribute("calendarName", calendar.getName());
        model.addAttribute("allEvents", eventArray);
        model.addAttribute("calendar", calendar);
        model.addAttribute("event", new Event());
        model.addAttribute("doctor", doctor);

        return "calendar/calendar";
    }

    @PostMapping(value = "/calendar/{doctorId}", params = "action=edit")
    public String bookAppointment( String oldDate,@PathVariable Long doctorId, Principal principal) {
        Doctor doctor = doctorService.findDoctorById(doctorId);
        User loggedinUser = userService.retrieveByEmail(principal.getName());
        Patient patient = patientService.retrieveByUser(loggedinUser);

        String[] dateTimeParts = oldDate.split("T");
        LocalDate localDate = LocalDate.parse(dateTimeParts[0]);
        LocalTime localTime = LocalTime.parse(dateTimeParts[1]);
        be.multimedi.latte.dbno.model.Calendar calendar = calendarRepository.findByDoctor(doctor);
        Event updatedEvent = eventService.findByCalendarAndStartDateAndStartTime(calendar, localDate, localTime);
        updatedEvent.setPatient(patient);
        updatedEvent.setStatus(Status.UNAVAILABLE);
        eventService.updateEvent(updatedEvent);
        emailService.sendNewAppointmentScheduledNotification(updatedEvent);
        patient.getUser().addEvent(updatedEvent);
        patientService.editPatient(patient);
        return "redirect:/patient/calendar/" + doctor.getId();
    }

    @PostMapping("/calendar/{doctorId}/cancel-appointment/{eventId}")
    public String cancelAppointment(@PathVariable Long doctorId, @PathVariable Long eventId, Principal principal){
        Doctor doctor = doctorService.findDoctorById(doctorId);
        User loggedinUser = userService.retrieveByEmail(principal.getName());
        Patient patient = patientService.retrieveByUser(loggedinUser);

        Event event = eventService.findById(eventId);
        emailService.sendAppointmentCanceledNotification(event);
        event.setPatient(null);
        event.setStatus(Status.AVAILABLE);
        eventService.updateEvent(event);
        patient.getUser().removeEventFromList(event);
        patientService.editPatient(patient);

//        return "redirect:/patient/appointment/all";
        return "redirect:/patient/calendar/" + doctor.getId();
    }

    private JsonArray getCalendarEventArray(List<Event> allEvents) {

        JsonArray eventArray = new JsonArray();

        for (int i = 0; i < allEvents.size(); i++) {
            JsonObject eventJson = new JsonObject();
            eventJson.addProperty("title", allEvents.get(i).getTitle());
            eventJson.addProperty("start", allEvents.get(i).getDate() + "T" +
                    allEvents.get(i).getStartTime());
            eventJson.addProperty("end", allEvents.get(i).getDate() + "T" +
                    allEvents.get(i).getEndTime());
            eventJson.addProperty("status", allEvents.get(i).getStatus().name());
            // eventJson.addProperty("patient", allEvents.get(i).getPatient().getUser().getEmail());
            eventArray.add(eventJson);
        }
        return eventArray;
    }


    @GetMapping("/appointment/all")
    public String showMyAppointmentsOnCalendar(Model model, Principal principal){

        Patient patient = patientService.retrieveByUser(userService.retrieveByEmail(principal.getName()));
        List<Event> allEvents = eventService.findEventsByPatient(patient);

//        model.addAttribute("calendarName", calendar.getName());
        model.addAttribute("allEvents",  getCalendarEventArray(allEvents));
//        model.addAttribute("calendar", calendar);
        model.addAttribute("event", new Event());
//        model.addAttribute("doctor", doctor);

        return "patient/patient-calendar";
    }


    @PostMapping(value = "/appointment", params = "action=edit")
    public String showAppointment(Principal principal, Model model,String oldDate){
        User loggedinUser = userService.retrieveByEmail(principal.getName());
        Patient patient = patientService.retrieveByUser(loggedinUser);
        String[] dateTimeParts = oldDate.split("T");
        LocalDate localDate = LocalDate.parse(dateTimeParts[0]);
        LocalTime localTime = LocalTime.parse(dateTimeParts[1]);
        Event event = eventService.findEventByPatientAndByDateAndByStartTime(patient,localDate,localTime);
        model.addAttribute("event", event);
        return "patient/patient-appointment";
    }



}
