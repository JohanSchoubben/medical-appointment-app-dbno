
package be.multimedi.latte.dbno.controller;

import be.multimedi.latte.dbno.DTO.DoctorDto;
import be.multimedi.latte.dbno.model.*;
import be.multimedi.latte.dbno.repository.CalendarRepository;
import be.multimedi.latte.dbno.service.DoctorService;
import be.multimedi.latte.dbno.service.EventService;
import be.multimedi.latte.dbno.service.OfficeService;
import be.multimedi.latte.dbno.service.UserService;
import be.multimedi.latte.dbno.util.OnRegistrationCompleteEvent;
import be.multimedi.latte.dbno.util.UserAlreadyExistException;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/doctor")
public class DoctorController {

    private final DoctorService doctorService;
    private final UserService userService;
    private final EventService eventService;
    private final CalendarRepository calendarRepository;
    private final OfficeService officeService;
    private final PasswordEncoder encoder;

    private Doctor findCurrentDoctor(Principal principal) {
        User loggedInUser = userService.retrieveByEmail(principal.getName());
        Doctor doctor = doctorService.findByUser(loggedInUser);
        return doctor;
    }

    @Autowired
    ApplicationEventPublisher eventPublisher;


    @GetMapping("/register")
    public String registerViewDoctor(WebRequest request, Model model) {
        DoctorDto doctorDto = new DoctorDto();
        model.addAttribute("doctor", doctorDto);
        return "doctorRegister";
    }


    @PostMapping("/register")
    public ModelAndView registerUserAccount(
            @ModelAttribute("user") @Valid DoctorDto doctorDto,
            HttpServletRequest request, Errors errors) {

        try {
            Doctor registered = doctorService.registerNewUserAccount(doctorDto);
            String appUrl = request.getContextPath();
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered.getUser(),
                    request.getLocale(), appUrl));
        } catch (UserAlreadyExistException uaeEx) {
            ModelAndView mav = new ModelAndView("doctorRegister", "doctor", doctorDto);
            mav.addObject("message", "An account for that username/email already exists.");
            return mav;
        } catch (RuntimeException ex) {
            ex.printStackTrace();
            return new ModelAndView("emailError", "doctor", doctorDto);
        }

        return new ModelAndView("home", "doctor", doctorDto);
    }


    @GetMapping("/my-profile/edit")
    public String editProfileView(Model model, Principal principal){
        User loggedInUser = userService.retrieveByEmail(principal.getName());
        Doctor doctor = doctorService.findByUser(loggedInUser);
        doctor.getUser().setPassword("");
        model.addAttribute("doctor", doctor);
        return "doctor/profileForm";
    }

    @PostMapping("/my-profile/edit")
    public String updateProfile(@ModelAttribute("doctor") @Valid Doctor updatedDoctor, BindingResult br, Principal principal){ //Principal = is information about 'current logged in user'
        if (br.hasErrors()){
            return "doctor/profileForm";
        }
        User loggedInUser = userService.retrieveByEmail(principal.getName());
        Doctor doctor = doctorService.findByUser(loggedInUser);
        String oldPassword = updatedDoctor.getUser().getPassword();
        String newPassword = encoder.encode(oldPassword);
        updatedDoctor.getUser().setPassword(newPassword);
        boolean allowedToUpdate = doctor.getId().equals(updatedDoctor.getId());
        if (allowedToUpdate){
            doctorService.updateUser(updatedDoctor);
        }
        return "redirect:/my-profile?updated";
    }


//    @GetMapping("/register")
//    public String registerViewDocter(Model model) {
//        model.addAttribute("doctor", new Doctor());
//        return "doctorRegister";
//    }
//
//    @PostMapping("/register")
//    public String registerDoctor(@ModelAttribute("doctor") @Valid final Doctor doctor, final BindingResult bindingResult) {
//
//        if (bindingResult.hasErrors()) {
//            return "doctorRegister";
//        }
//        doctorService.register(doctor);
//        return "redirect:/login";
//    }

    @GetMapping("/office/new")
    public String showAddOfficeForm(Model model) {
        model.addAttribute("office", new Office());
        return "office-form";
    }

    @PostMapping("/office/new")
    public String addOffice(@Valid Office office, BindingResult br, Principal principal) {
        if (br.hasErrors()) {
            return "office-form";
        } else {
            User loggedInUser = userService.retrieveByEmail(principal.getName());
            Doctor doctor = doctorService.findByUser(loggedInUser);
            officeService.addNewOffice(doctor, office);
            return "home";
        }
    }

    @GetMapping("/{id}")
    public String showDoctorDetails(@PathVariable Long id, Model model) {
        Doctor doctor = doctorService.findDoctorById(id);
        model.addAttribute("doctor", doctor);
        return "doctor/dr-detail";
    }

    @GetMapping("/mydetails")
    public String myDoctorDetails(Model model, Principal principal) {
        User loggedInUser = userService.retrieveByEmail(principal.getName());
        Doctor doctor = doctorService.findByUser(loggedInUser);
        model.addAttribute("doctor", doctor);
        return "doctor/dr-detail";
    }

    @GetMapping("/{id}/calendar")
// @RequestParam ******
    public String showDrCalendar(Model model, @PathVariable Long id, @RequestParam(defaultValue = "ALL") Status status) {

        Doctor doctor = doctorService.findDoctorById(id);
        if (doctor.getCalendar() == null) {
            addCalendar(doctor);
        }
        Calendar calendar = calendarRepository.findByDoctor(doctor);
//        Calendar calendar = calendarRepository.findByDoctor(doctorService.findDoctorById(doctorId));
        checkEventDate(calendar);
        //All events to current doctor
        //added ******
        List<Event> allEvents = status == Status.ALL ?
                eventService.findAllByCalendar(calendar) :
                eventService.findAllByCalendarAndStatus(calendar, status);

        //Info to fill out calendar.
        JsonArray eventArray = new JsonArray();

        for (int i = 0; i < allEvents.size(); i++) {
            JsonObject eventJson = new JsonObject();
            eventJson.addProperty("title", allEvents.get(i).getTitle());
            eventJson.addProperty("start", allEvents.get(i).getDate() + "T" +
                    allEvents.get(i).getStartTime());
//            eventJson.addProperty("end", allEvents.get(i).getDate() + "T" +
//                    allEvents.get(i).getEndTime());
            eventJson.addProperty("status", allEvents.get(i).getStatus().name());
            // eventJson.addProperty("patient", allEvents.get(i).getPatient().getUser().getEmail());
            eventArray.add(eventJson);
        }
        model.addAttribute("calendarName", calendar.getName());
        model.addAttribute("currentUser", doctor);
        model.addAttribute("allEvents", eventArray);
        model.addAttribute("calendar", calendar);
        model.addAttribute("event", new Event());
        model.addAttribute("doctor", doctor);

        return "calendar/calendar";
    }

    private void checkEventDate(Calendar calendar) {

        List<Event> events = eventService.findAllByCalendar(calendar);

        for(Event event : events){
            if(event.getDate().isBefore(LocalDate.now()) || (!event.getDate().isAfter(LocalDate.now()) && event.getStartTime().isBefore(LocalTime.now()))){
                event.setStatus(Status.UNAVAILABLE);
                eventService.updateEvent(event);
            }
        }
    }


    @PostMapping(value = "/{id}/calendar", params = "action=delete")
    public String eventDelete(String oldDate, @PathVariable Long id) {
        Doctor doctor = doctorService.findDoctorById(id);

        String[] dateTimeParts = oldDate.split("T");
        LocalDate localDate = LocalDate.parse(dateTimeParts[0]);
        LocalTime localTime = LocalTime.parse(dateTimeParts[1]);
        Calendar calendar = calendarRepository.findByDoctor(doctor);
        Event e1 = eventService.findByCalendarAndStartDateAndStartTime(calendar, localDate, localTime);
        doctor.getUser().removeEventFromList(e1);
        doctorService.editDoctor(doctor);
        eventService.delete(e1);

        return "redirect:/doctor/" + doctor.getId() + "/calendar";
    }



    private void addCalendar(Doctor doctor) {
        Calendar calendar = new Calendar();
        calendar.setName("Dr. " + doctor.getUser().getLastName() + " - " + doctor.getSpecialization().getValue());
        calendar.setDoctor(doctor);
        doctor.setCalendar(calendar);
        calendarRepository.save(calendar);
        doctorService.editDoctor(doctor);
    }

    @GetMapping("/calendar/event/new")
    public String showEvent(Model model, Principal principal, Event event, String calName) {
        Doctor doctor = findCurrentDoctor(principal);
        //If user doesnt have calendar - redirected to create
        try {
            calendarRepository.findByDoctor(doctor);
        } catch (Exception e) {
            return "redirect:/doctor/calendar/new";
        }
        Calendar calendar = calendarRepository.findByDoctor(doctor);

        JsonObject calendarJson = new JsonObject();
        calendarJson.addProperty("name", calendar.getName());


        model.addAttribute("calendar", calendarJson);

        return "calendar/add-event";
    }

    @PostMapping("/calendar/event/new")
    public String addEvent(Model model, Principal principal, @Valid Event event, BindingResult bindingResult, String calName) {
        //Gets all the calendars and eventTypes again and sends them to view.
        //--

        Doctor doctor = findCurrentDoctor(principal);

        Calendar calendar = calendarRepository.findByDoctor(doctor);

        JsonObject calendarJson = new JsonObject();
        calendarJson.addProperty("name", calendar.getName());

        model.addAttribute("calendar", calendarJson);

        //--

//
//        Calendar c1 = calendarRepo.findByNameAndUser(calName, currUser);
//        if(c1 == null)
//        {
//            c1 = calendarRepo.findByNameAndUser(currUser.getActiveCalendar(), currUser);
//        }
        event.setCalendar(calendar);

        //     eventValidator.validate(event, bindingResult);

        if (bindingResult.hasErrors()) {
            return "calendar/add-event";
        }
        eventService.save(event);
        doctor.getUser().addEvent(event);
        doctorService.editDoctor(doctor);

        return "redirect:/doctor/" + doctor.getId() + "/calendar";
    }


}
