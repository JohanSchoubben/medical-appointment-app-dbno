package be.multimedi.latte.dbno.repository;

import be.multimedi.latte.dbno.model.Calendar;
import be.multimedi.latte.dbno.model.Event;
import be.multimedi.latte.dbno.model.Patient;
import be.multimedi.latte.dbno.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public interface EventRepository extends JpaRepository<Event, Long> {

    Event findEventByCalendarAndDateAndStartTime(Calendar calendar, LocalDate localDate, LocalTime localTime);

    ArrayList<Event> findAllByCalendar(Calendar calendar);

    ArrayList<Event> findAllByCalendarAndStatus(Calendar calendar, Status status);

    List<Event> findAllByPatient(Patient patient);

    Event findEventByPatientAndDateAndStartTime(Patient patient,LocalDate localDate, LocalTime localTime);
}