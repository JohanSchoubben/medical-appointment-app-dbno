package be.multimedi.latte.dbno.repository;

import be.multimedi.latte.dbno.model.Doctor;
import be.multimedi.latte.dbno.model.Specialization;
import be.multimedi.latte.dbno.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {

//    List<Doctor> findDoctorByLastNameIsLike(String searchTerm);

    // @Query(value = "SELECT * FROM DOCTOR D JOIN USER U ON U.ID = D.USER_ID WHERE U.FIRST_NAME LIKE :name AND D.SPECIALIZATION = :spec", nativeQuery = true)
//    @Query("select d from Doctor d where d.user.lastName like :name and d.specialization = :spec")

    @Query("select d from Doctor d where d.user.lastName like :name and (d.specialization = :spec or :spec is null or :spec = '' )")
    List<Doctor> findBySpecializationAndName(String name, Specialization spec);

    Doctor findByUser(User user);
}
