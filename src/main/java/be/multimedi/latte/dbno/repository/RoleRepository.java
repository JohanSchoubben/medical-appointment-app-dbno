package be.multimedi.latte.dbno.repository;

import be.multimedi.latte.dbno.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(String name);
}
