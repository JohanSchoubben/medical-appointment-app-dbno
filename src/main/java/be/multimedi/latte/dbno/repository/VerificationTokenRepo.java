package be.multimedi.latte.dbno.repository;

import be.multimedi.latte.dbno.model.Patient;
import be.multimedi.latte.dbno.security.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerificationTokenRepo extends JpaRepository<VerificationToken, Long> {

VerificationToken findVerificationTokenByToken(String token);


}
