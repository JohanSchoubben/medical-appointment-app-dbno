package be.multimedi.latte.dbno.repository;

import be.multimedi.latte.dbno.model.Patient;
import be.multimedi.latte.dbno.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {
    Patient findByUser(User user);
}
