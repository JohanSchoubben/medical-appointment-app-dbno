package be.multimedi.latte.dbno.repository;

import be.multimedi.latte.dbno.model.Calendar;
import be.multimedi.latte.dbno.model.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CalendarRepository extends JpaRepository<Calendar, Long> {
    Calendar findByDoctor(Doctor doctor);
}