package be.multimedi.latte.dbno.util;

public class UserAlreadyExistException extends RuntimeException{

    private String email;

    public UserAlreadyExistException(String email) {
        this.email = email;
    }

    @Override
    public String getMessage() {
        return String.format("the following email : " + email + " is already in use \n");
    }
}
