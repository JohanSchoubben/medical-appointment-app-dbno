package be.multimedi.latte.dbno.util;

import be.multimedi.latte.dbno.model.Calendar;
import be.multimedi.latte.dbno.model.Doctor;
import be.multimedi.latte.dbno.model.Event;
import be.multimedi.latte.dbno.model.Status;
import be.multimedi.latte.dbno.repository.CalendarRepository;
import be.multimedi.latte.dbno.service.DoctorService;
import be.multimedi.latte.dbno.service.EventService;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

@Aspect
@Component
@RequiredArgsConstructor
public class AspectService {
    private final DoctorService doctorService;
    private final CalendarRepository calendarRepository;
    private final EventService eventService;

    @Before("execution(* be.multimedi.latte.dbno.controller.*.showDrCalendar(..))")
    public void checkEventDate(JoinPoint joinPoint) {
        Long id = (Long) joinPoint.getArgs()[1];
        Doctor doctor = doctorService.findDoctorById(id);
        Calendar calendar = calendarRepository.findByDoctor(doctor);
        List<Event> events = eventService.findAllByCalendar(calendar);

        for (Event event : events) {
            ZoneId zone = ZoneId.of("Europe/Paris");
            ZonedDateTime zonedEventDateTime = ZonedDateTime.of(event.getDate(), event.getStartTime(), zone);
            boolean eventDateTimeIsPast = zonedEventDateTime.isBefore(ZonedDateTime.now());
            if ( eventDateTimeIsPast) {
                event.setStatus(Status.UNAVAILABLE);
                eventService.updateEvent(event);
            }
        }
    }
}
