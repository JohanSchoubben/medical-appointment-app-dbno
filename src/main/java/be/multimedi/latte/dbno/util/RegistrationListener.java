package be.multimedi.latte.dbno.util;

import be.multimedi.latte.dbno.model.User;
import be.multimedi.latte.dbno.service.MailService;
import be.multimedi.latte.dbno.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class RegistrationListener implements
        ApplicationListener<OnRegistrationCompleteEvent> {
    private final UserService userService;
    private final MailService mailService;
    @Value(value = "${appurl}")
    private String appUrl;

    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(OnRegistrationCompleteEvent event) {
        User user = event.getUser();
        String token = UUID.randomUUID().toString();
        userService.createVerificationToken(user, token);
        String subject = "Bevestig uw registratie op DBNO";
        String message = "Bevestig uw account op DBNO via deze link:\n";
        String confirmationUrl
                = "/registrationConfirm?token=" + token;
        String messageText = message + appUrl + confirmationUrl;
        mailService.sendMail(user, subject, messageText);
    }
}
