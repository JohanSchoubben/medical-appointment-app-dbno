package be.multimedi.latte.dbno.DTO;

import be.multimedi.latte.dbno.annotations.PasswordMatches;
import be.multimedi.latte.dbno.annotations.ValidEmail;
import be.multimedi.latte.dbno.model.Address;
import be.multimedi.latte.dbno.model.Gender;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@PasswordMatches
public class UserDto {


    @NotNull
    @NotEmpty
    private String firstName;

    @NotNull
    @NotEmpty
    private String lastname;

    @NotNull
    private Gender gender;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dob;

    @NotNull
    @NotEmpty
    private String phoneNumber;

    @NotNull
    private Address address;

    @NotNull
    @NotEmpty
    private String nationalRegisterNumber;

    @ValidEmail
    @NotNull
    @NotEmpty
    private String email;

    @NotNull
    @NotEmpty
    private String password;


    @NotNull
    @NotEmpty
    private String confirmPassword;




}
