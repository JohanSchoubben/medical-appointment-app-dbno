package be.multimedi.latte.dbno.DTO;

import be.multimedi.latte.dbno.model.Specialization;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@Getter
@Setter
public class DoctorDto {


    @NotNull
    private UserDto userDto;

    @NotNull
    private Specialization specialization;



    //confirmPassword used to live here , place it back if signs of trouble

}
