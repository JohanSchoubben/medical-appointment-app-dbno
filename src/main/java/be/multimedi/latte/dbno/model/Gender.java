package be.multimedi.latte.dbno.model;

public enum Gender {
    MALE("male"), FEMALE("female"), OTHER("other");


    private final String displayValue;

    private Gender(String displayValue) {
        this.displayValue = displayValue;
    }


    public String getDisplayValue() {
        return displayValue;
    }
}
