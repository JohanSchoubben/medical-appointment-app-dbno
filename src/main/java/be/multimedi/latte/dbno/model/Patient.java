package be.multimedi.latte.dbno.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;




@Getter
@Setter
@Entity
@NoArgsConstructor
public class Patient {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    private User user;


    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", user=" + user +
                '}';
    }
}
