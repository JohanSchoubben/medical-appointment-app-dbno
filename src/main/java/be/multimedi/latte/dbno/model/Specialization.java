package be.multimedi.latte.dbno.model;

public enum Specialization {
    GENERAL_PRACTIONER("Huisarts"),
    SURGEON("Chirurg"),
    CARDIOLOGIST("Cardioloog"),
    NEUROLOGIST("Neuroloog"),
    RADIOLOGIST("Radioloog"),
    ONCOLOGIST("Oncoloog"),
    UROLOGIST("Uroloog"),
    GYNECOLOGIST("Gynaecoloog"),
    PAEDIATRICIAN("Kinderarts"),
    DERMATOLOGIST("Dermatoloog");

    private String value;

    Specialization(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
