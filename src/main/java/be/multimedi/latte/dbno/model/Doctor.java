package be.multimedi.latte.dbno.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class Doctor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private User user;
    @Enumerated(value = EnumType.STRING)
    private Specialization specialization;
    @ManyToMany
    @JoinTable(
            name = "doctor_offices",
            joinColumns = @JoinColumn(name = "doctor_id"),
            inverseJoinColumns = @JoinColumn(name = "office_id"))
    private List<Office> offices = new ArrayList<>();
    @OneToOne
    private Calendar calendar;


}
