package be.multimedi.latte.dbno.model;

import be.multimedi.latte.dbno.security.VerificationToken;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dob;
    private String phoneNumber;
    @OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Address address;
    private String nationalRegisterNumber;
    @Email(message = "Dit is geen geldig e-mailadres")
    private String email;
    @Enumerated(value = EnumType.STRING)
    private Gender gender;
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")
    private String password;
    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private Set<Role> roles = new HashSet<>();
    @OneToOne(mappedBy = "user")
    private VerificationToken verificationToken;
    @OneToMany
    private List<Event> events = new ArrayList<>();



    public User(String firstName,
                String lastName,
                LocalDate dob,
                String phoneNumber,
                Address address,
                String nationalRegisterNumber,
                String email,
                String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.nationalRegisterNumber = nationalRegisterNumber;
        this.email = email;
        this.password = password;
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public void addRole(Role role){
        this.roles.add(role);
    }


    @Column(name = "enabled")
    private boolean enabled;

    public User() {
        super();
        this.enabled=false;
    }


    public void addEvent(Event event){
        if(!this.events.contains(event)){
            this.events.add(event);
        }
    }

    public void removeEventFromList(Event event) {
        if (this.events.contains(event)) {
        this.events.remove(event);
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dob=" + dob +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address=" + address +
                ", nationalRegisterNumber='" + nationalRegisterNumber + '\'' +
                ", email='" + email + '\'' +
                ", gender=" + gender +
                '}';
    }
}
