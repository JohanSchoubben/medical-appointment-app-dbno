package be.multimedi.latte.dbno.security;

import be.multimedi.latte.dbno.model.Patient;
import be.multimedi.latte.dbno.model.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

@Entity
@Getter
@Setter


public class VerificationToken {

    private static final int EXPIRATION = 60 * 24;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String token;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "User_id")
    private User user;

    private Date expiryDate;

    public void calculateExpiryDate(int expiryTimeInMinutes) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Timestamp(cal.getTime().getTime())); //java sql , is this correct ?
        cal.add(Calendar.MINUTE, expiryTimeInMinutes);

        this.expiryDate = new Date(cal.getTime().getTime());
    }
}
