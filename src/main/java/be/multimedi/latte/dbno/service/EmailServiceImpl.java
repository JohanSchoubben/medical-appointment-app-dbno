package be.multimedi.latte.dbno.service;

import be.multimedi.latte.dbno.model.Event;
import be.multimedi.latte.dbno.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {
    private final JavaMailSender mailSender;

    @Async
    @Override
    public void sendEmail(String to, String subject, String text) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setSubject(subject);
        email.setTo(to);
        email.setText(text);
        mailSender.send(email);
    }

    @Async
    @Override
    public void sendNewAppointmentScheduledNotification(Event appointment) {
        User doctor = appointment.getCalendar().getDoctor().getUser();
        User patient = appointment.getPatient().getUser();
        String text = "Beste meneer/mevrouw " + patient.getLastName() + ". Uw afspraak is succesvol aangemaakt." +
                " Datum : " + appointment.getDate() + " Tijd : " + appointment.getStartTime() +
                " Duur : 15 min. Dokter " + doctor.getFirstName() + " " + doctor.getLastName() + " " + doctor.getAddress().toString();

        sendEmail(patient.getEmail(), "Nieuwe afspraak geboekt", text);
    }

    @Async
    @Override
    public void sendAppointmentCanceledNotification(Event appointment) {
        User doctor = appointment.getCalendar().getDoctor().getUser();
        User patient = appointment.getPatient().getUser();
        String text = "Beste meneer/mevrouw " + patient.getLastName() + ". Uw afspraak is geannuleerd." +
                " Datum : " + appointment.getDate() + " Tijd : " + appointment.getStartTime() +
                " Duur : 15 min. Dokter " + doctor.getFirstName() + " " + doctor.getLastName() + " " + doctor.getAddress().toString();

        sendEmail(patient.getEmail(), "Afspraak geannuleerd", text);
    }
}
