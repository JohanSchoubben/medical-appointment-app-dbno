package be.multimedi.latte.dbno.service;

import be.multimedi.latte.dbno.model.User;
import be.multimedi.latte.dbno.security.VerificationToken;

public interface UserService {
    VerificationToken getVerificationToken(String token) ;



    User retrieveByEmail(String username);

    void createVerificationToken(User user, String token);

    void saveRegisteredUser(User user);
}
