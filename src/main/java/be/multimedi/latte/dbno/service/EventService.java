package be.multimedi.latte.dbno.service;

import be.multimedi.latte.dbno.model.Calendar;
import be.multimedi.latte.dbno.model.Event;
import be.multimedi.latte.dbno.model.Patient;
import be.multimedi.latte.dbno.model.Status;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public interface EventService {

    Event findByCalendarAndStartDateAndStartTime(Calendar calendar, LocalDate localDate, LocalTime localTime);
    Event findById(Long eventId);

    ArrayList<Event> findAllByCalendar(Calendar calendar);
    //added
    ArrayList<Event> findAllByCalendarAndStatus(Calendar calendar, Status status);

    void save(Event e1);

    void delete(Event e1);
    void saveAllEvents(List<Event> events);
    void updateEvent(Event event);

    List<Event> findEventsByPatient(Patient patient);

    Event findEventByPatientAndByDateAndByStartTime(Patient patient, LocalDate localDate, LocalTime localTime);
}
