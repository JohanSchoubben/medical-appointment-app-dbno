package be.multimedi.latte.dbno.service;

import be.multimedi.latte.dbno.DTO.DoctorDto;
import be.multimedi.latte.dbno.DTO.PatientDto;
import be.multimedi.latte.dbno.model.*;
import be.multimedi.latte.dbno.repository.DoctorRepository;
import be.multimedi.latte.dbno.repository.RoleRepository;
import be.multimedi.latte.dbno.repository.UserRepository;
import be.multimedi.latte.dbno.util.UserAlreadyExistException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DoctorServiceImpl implements DoctorService {
    private final DoctorRepository doctorRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder pwdEncoder;
    private final UserRepository userRepository;

    @Override
    public void register(Doctor doctor) {

        final User user = doctor.getUser();
        final String encodedPwd = pwdEncoder.encode(user.getPassword());
        user.setPassword(encodedPwd);
        Role role = roleRepository.findById(2L).orElseThrow(EntityNotFoundException::new);
        user.addRole(role);
        doctor.setUser(user);
        doctorRepository.save(doctor);
    }
    @Override
    public List<Doctor> search(String name, Specialization spec) {

        return doctorRepository.findBySpecializationAndName("%"+name+"%", spec);
    }

    //    @Override
//    public List<Doctor> searchByLastName(String searchTerm) {
//        return doctorRepository.findDoctorByLastNameIsLike("%" + searchTerm + "%");
//    }
//}


    @Override
    public Doctor registerNewUserAccount(DoctorDto doctorDto) throws UserAlreadyExistException {


        if (emailExist(doctorDto.getUserDto().getEmail())) {
            throw new UserAlreadyExistException("There is an account with that email address: "
                    + doctorDto.getUserDto().getEmail());
        }


        User user = new User();
        user.setFirstName(doctorDto.getUserDto().getFirstName());
        user.setLastName(doctorDto.getUserDto().getLastname()); // conventie , pas last name aan (Hoofdletter N
        user.setGender(doctorDto.getUserDto().getGender());
        user.setAddress(doctorDto.getUserDto().getAddress());
        user.setNationalRegisterNumber(doctorDto.getUserDto().getNationalRegisterNumber());
        user.setPhoneNumber(doctorDto.getUserDto().getPhoneNumber());
        user.setDob(doctorDto.getUserDto().getDob());
        user.setPassword(doctorDto.getUserDto().getPassword());
        user.setEmail(doctorDto.getUserDto().getEmail());

        Doctor doctor = new Doctor();
        doctor.setSpecialization(doctorDto.getSpecialization());
        final String encodedPwd = pwdEncoder.encode(user.getPassword());
        user.setPassword(encodedPwd);
        Role role = roleRepository.findById(2L).orElseThrow(EntityNotFoundException::new);
        user.addRole(role);
        doctor.setUser(user);


        return doctorRepository.save(doctor);
    }

    @Override
    public void updateUser(Doctor updatedDoctor) {
        doctorRepository.save(updatedDoctor);
    }

    private boolean emailExist(String email) {
        return userRepository.findByEmail(email).isPresent();}


    @Override
    public List<Doctor> getAllDoctors() {
        return doctorRepository.findAll();
    }

    @Override
    public void editDoctor(Doctor doctor) {
        doctorRepository.save(doctor);
    }

    @Override
    public void deleteDoctorById(Long id) {
        doctorRepository.deleteById(id);
    }

    @Override
    public Doctor findDoctorById(Long id) {
        return doctorRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public Doctor findByUser(User user){
        return doctorRepository.findByUser(user);
    }
}
