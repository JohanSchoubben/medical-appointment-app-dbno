package be.multimedi.latte.dbno.service;

import be.multimedi.latte.dbno.DTO.DoctorDto;
import be.multimedi.latte.dbno.model.Doctor;
import be.multimedi.latte.dbno.model.Specialization;
import be.multimedi.latte.dbno.model.User;

import java.util.List;

import java.util.List;

public interface DoctorService {
    public void register(Doctor doctor);
    List<Doctor> search(String name, Specialization spec);

//    List<Doctor> searchByLastName(String searchTerm);

    public List<Doctor> getAllDoctors();
    public void editDoctor(Doctor doctor);
    public void deleteDoctorById(Long id);
    public Doctor findDoctorById(Long id);
    public Doctor findByUser(User user);
     Doctor registerNewUserAccount(DoctorDto doctorDto);

    void updateUser(Doctor updatedDoctor);
}
