package be.multimedi.latte.dbno.service;

import be.multimedi.latte.dbno.model.Calendar;
import be.multimedi.latte.dbno.model.Event;
import be.multimedi.latte.dbno.model.Patient;
import be.multimedi.latte.dbno.model.Status;
import be.multimedi.latte.dbno.repository.EventRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


@Service
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {
    private final EventRepository eventRepository;
    @Override
    public Event findByCalendarAndStartDateAndStartTime(Calendar calendar, LocalDate localDate, LocalTime localTime) {
        return eventRepository.findEventByCalendarAndDateAndStartTime(calendar,localDate,localTime);
    }

    @Override
    public Event findById(Long eventId) {
        return eventRepository.findById(eventId).orElseThrow(NoSuchElementException::new);
    }

    @Override
    public ArrayList<Event> findAllByCalendar(Calendar calendar) {
        return eventRepository.findAllByCalendar(calendar);
    }
    // added
    @Override
    public ArrayList<Event> findAllByCalendarAndStatus(Calendar calendar, Status status) {
        return eventRepository.findAllByCalendarAndStatus(calendar,status);
    }

    @Override
    public void save(Event e1) {
        eventRepository.save(e1);
    }

    @Override
    public void delete(Event e1) {
        eventRepository.delete(e1);
    }

    @Override
    public void saveAllEvents(List<Event> events) {
        eventRepository.saveAll(events);
    }

    @Override
    public void updateEvent(Event event) {
        eventRepository.save(event);
    }


    @Override
    public List<Event> findEventsByPatient(Patient patient) {
        return eventRepository.findAllByPatient(patient);
    }

    @Override
    public Event findEventByPatientAndByDateAndByStartTime(Patient patient, LocalDate localDate, LocalTime localTime) {
        return eventRepository.findEventByPatientAndDateAndStartTime(patient, localDate, localTime);
    }
}
