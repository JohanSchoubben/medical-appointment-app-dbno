package be.multimedi.latte.dbno.service;

import be.multimedi.latte.dbno.model.Doctor;
import be.multimedi.latte.dbno.model.Office;
import be.multimedi.latte.dbno.repository.DoctorRepository;
import be.multimedi.latte.dbno.repository.OfficeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OfficeServiceImpl implements OfficeService{
   private final OfficeRepository officeRepository;
   private final DoctorRepository doctorRepository;

   @Override
   public void addNewOffice(Doctor doctor, Office office) {
      doctor.getOffices().add(office);
      office.getDoctors().add(doctor);
      officeRepository.save(office);
      doctorRepository.save(doctor);
   }
}
