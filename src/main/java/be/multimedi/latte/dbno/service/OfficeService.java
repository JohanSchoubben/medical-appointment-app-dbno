package be.multimedi.latte.dbno.service;

import be.multimedi.latte.dbno.model.Doctor;
import be.multimedi.latte.dbno.model.Office;

public interface OfficeService {

    void addNewOffice(Doctor doctor, Office office);
}
