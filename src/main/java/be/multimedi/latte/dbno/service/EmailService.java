package be.multimedi.latte.dbno.service;

import be.multimedi.latte.dbno.model.Event;

public interface EmailService {

    void sendEmail(String to, String subject,String text);
    void sendNewAppointmentScheduledNotification(Event appointment);
    void sendAppointmentCanceledNotification(Event appointment);
}
