package be.multimedi.latte.dbno.service;

import be.multimedi.latte.dbno.configuration.MailProperties;
import be.multimedi.latte.dbno.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@RequiredArgsConstructor
public class MailServiceImplementation implements MailService {
    private final JavaMailSender javaMailSender;
    private final MailProperties mailProperties;

    @Override
    public void sendMail(User user, String subject, String messageText) {
        System.out.println("sending e-mail starts ...");
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(mailProperties.getUsername());
        simpleMailMessage.setTo(user.getEmail());
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(messageText);
        simpleMailMessage.setSentDate(new Date());
        javaMailSender.send(simpleMailMessage);
        System.out.println("sent ...");
    }
}
