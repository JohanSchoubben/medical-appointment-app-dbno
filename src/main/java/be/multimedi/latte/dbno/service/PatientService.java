package be.multimedi.latte.dbno.service;


import be.multimedi.latte.dbno.DTO.PatientDto;
import be.multimedi.latte.dbno.model.Patient;
import be.multimedi.latte.dbno.model.User;

import java.util.List;

public interface PatientService {
    Patient registerNewUserAccount(PatientDto patientDto);
    void register(Patient patient);
    public List<Patient> getAllPatients();
    public void editPatient(Patient patient);
    public void deletePatientById(Long id);
    public Patient findPatientById(Long id);
    Patient retrieveByUser(User user);
    void updateUser(Patient updatedPatient);
}
