package be.multimedi.latte.dbno.service;

import be.multimedi.latte.dbno.model.User;
import be.multimedi.latte.dbno.repository.UserRepository;
import be.multimedi.latte.dbno.repository.VerificationTokenRepo;
import be.multimedi.latte.dbno.security.VerificationToken;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepo;
    private final VerificationTokenRepo verificationTokenRepo;


    @Override
    public VerificationToken getVerificationToken(String token) {
        return verificationTokenRepo.findVerificationTokenByToken(token);
    }

    @Override
    public User retrieveByEmail(String username) {
        return userRepo.findByEmail(username)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void createVerificationToken(User user, String token) {

        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setToken(token);
        verificationToken.setUser(user);
        verificationToken.calculateExpiryDate(1440);
        verificationTokenRepo.save(verificationToken);

    }

    @Override
    public void saveRegisteredUser(User user) {
        userRepo.save(user);
    }

}
