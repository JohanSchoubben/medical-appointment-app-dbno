package be.multimedi.latte.dbno.service;

import be.multimedi.latte.dbno.model.User;

public interface MailService {
    void sendMail(User user, String subject, String messageText);
}
