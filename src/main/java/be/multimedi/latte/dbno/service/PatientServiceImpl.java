package be.multimedi.latte.dbno.service;

import be.multimedi.latte.dbno.DTO.PatientDto;
import be.multimedi.latte.dbno.util.UserAlreadyExistException;
import be.multimedi.latte.dbno.model.Patient;
import be.multimedi.latte.dbno.model.Role;
import be.multimedi.latte.dbno.model.User;
import be.multimedi.latte.dbno.repository.PatientRepository;
import be.multimedi.latte.dbno.repository.RoleRepository;
import be.multimedi.latte.dbno.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PatientServiceImpl implements PatientService {
    private final PatientRepository patientRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder pwdEncoder;
    private final UserRepository userRepository;

    @Override
    public void register(Patient patient) {

        final User user = patient.getUser();
        final String encodedPwd = pwdEncoder.encode(user.getPassword());
        user.setPassword(encodedPwd);
        Role role = roleRepository.findById(1L).orElseThrow(EntityNotFoundException::new);
        user.addRole(role);
        patient.setUser(user);

        patientRepository.save(patient);
    }

    @Override
    public Patient registerNewUserAccount(PatientDto patientDto) throws UserAlreadyExistException {


        if (emailExist(patientDto.getUserDto().getEmail())) {
            throw new UserAlreadyExistException("There is an account with that email address: "
                    + patientDto.getUserDto().getEmail());
        }


        User user = new User();
        user.setFirstName(patientDto.getUserDto().getFirstName());
        user.setLastName(patientDto.getUserDto().getLastname()); // conventie , pas last name aan (Hoofdletter N
        user.setGender(patientDto.getUserDto().getGender());
        user.setAddress(patientDto.getUserDto().getAddress());
        user.setNationalRegisterNumber(patientDto.getUserDto().getNationalRegisterNumber());
        user.setPhoneNumber(patientDto.getUserDto().getPhoneNumber());
        user.setDob(patientDto.getUserDto().getDob());
        user.setPassword(patientDto.getUserDto().getPassword());
        user.setEmail(patientDto.getUserDto().getEmail());

        Patient patient = new Patient();
        final String encodedPwd = pwdEncoder.encode(user.getPassword());
        user.setPassword(encodedPwd);
        Role role = roleRepository.findById(1L).orElseThrow(EntityNotFoundException::new);
        user.addRole(role);
        patient.setUser(user);


        return patientRepository.save(patient);

    }

    private boolean emailExist(String email) {
        return userRepository.findByEmail(email).isPresent();}

        @Override
    public List<Patient> getAllPatients() {
        return patientRepository.findAll();
    }

    @Override
    public void editPatient(Patient patient) {
        patientRepository.save(patient);
    }

    @Override
    public void deletePatientById(Long id) {
        patientRepository.deleteById(id);
    }

    @Override
    public Patient findPatientById(Long id) {
        return patientRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public Patient retrieveByUser(User user) {
        return patientRepository.findByUser(user);
    }
    @Override
    public void updateUser(Patient updatedPatient) {
//        If we we don't want to use MERGE we can use this:
//        userRepository.save(updatedPatient.getUser());
        patientRepository.save(updatedPatient);
    }

}
