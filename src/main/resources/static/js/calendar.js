$(document).ready(function() {
    let all = JSON.parse($("#events").val());
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        navLinks: true, // can click day/week names to navigate views
        editable: false,
        eventLimit: true, // allow "more" link when too many events
        timeFormat: 'H:mm',
        events: all,
        eventRender: function(info, el, view) {

          let modal = document.getElementById("myModal");

            // Get the <span> element that closes the modal
            let span = document.getElementsByClassName("close")[0];

            // When the user clicks the button, open the modal


            switch(info.status) {
                case "AVAILABLE":
                    el[0].style.backgroundColor = "#00b300"
                    break;
                case "UNAVAILABLE":
                    el[0].style.backgroundColor = "#cc00cc"
                    break;
            }

            // // let status = document.getElementById("eventAvailable");
            // console.log(status);
            // function checkValue(status){
            //  if(status.value === info.status){
            //      return true;
            //  }
            // }
            // console.log("available" + checkValue(status))
            // function openEvent(){
            //   const status = document.getElementById("eventAvailable");
            //     console.log(all);
            //   return all.filter(status.value === "AVAILABLE")
            //
            //   /*  all.filter(checkValue(status));
            //     console.log(all);*/
            // }
            //
            // // status = document.getElementById("eventUnavailable");
            //
            // console.log("unavailable" + checkValue(status))
            // function closedEvent(){
            //     const status = document.getElementById("eventUnavailable");
            //     console.log(all);
            //     return all.filter(status.value === "UNAVAILABLE");
            // /*    all.filter(checkValue(status));
            //     console.log(all);*/
            // }

            el[0].onclick = function() {
                modal.style.display = "block";
                document.getElementById("evTitle").textContent = info.title
                document.getElementById("evDate").textContent = info.start._i
                let oldDate = document.getElementById("evDate")
                document.getElementById("oldDateSbm").value = oldDate.textContent
            }

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() {
                modal.style.display = "none";
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }

        },

    });

});



