//Multi step register form
let tabIndex = 0;
showTab(tabIndex);
function showTab(n) {

   let x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    if (n === 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "inline";
    }
    if (n === (x.length - 1)) {
        document.getElementById("nextBtn").innerHTML = "Verzenden";
    } else {
        document.getElementById("nextBtn").innerHTML = "Volgende";
    }
    fixStepIndicator(n)
}

function nextPrev(n) {
    let x = document.getElementsByClassName("tab");
    if (n === 1 && !validateForm()) return false;
    x[tabIndex].style.display = "none";
    tabIndex = tabIndex + n;
    if (tabIndex >= x.length) {
        document.getElementById("regForm").submit();
        return false;
    }
    showTab(tabIndex);
}

function validateForm() {
    let x, y, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[tabIndex].getElementsByTagName("input");
    for (let i = 0; i < y.length; i++) {
        if (y[i].value === "") {
            y[i].className += " invalid";
            valid = false;
        } else if(y[i].id === "password" && (!isValidPassword(y[i].value))){
            y[i].className += " invalid";
            valid = false;
        } else if(y[i].id === "confirmPassword" && (confirmPassword.value.trim() !== password.value.trim())){
            alert("Paswoorden komen niet overeen!");
            y[i].className += " invalid";
            valid = false;
        }
    }
    if (valid) {
        document.getElementsByClassName("step")[tabIndex].className += " finish";
    }
    return valid;
}

function fixStepIndicator(n) {
    let x = document.getElementsByClassName("step");
    for (let i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    x[n].className += " active";
}
let pwd;
function isValidPassword(pwd) {
    let minNumberOfChars = 8;
    let regularExpression  = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
     if(pwd.length < minNumberOfChars || !pwd.match(regularExpression)) {
         alert("Paswoord moet ten minste 8 tekens, één cijfer en één hoofdletter bevatten");
         return false;
     }else{
         return true;
    }
}
